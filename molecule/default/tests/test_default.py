import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_mariadb_enabled_and_running(host):
    service = host.service('mariadb')
    assert service.is_running
    assert service.is_enabled
